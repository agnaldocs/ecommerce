<?php
namespace App\Controllers\Category;

use App\Models\Category;
use App\Controllers\Controller;

class CategoryController extends Controller
{

    private $categoria;

  	function __construct()
  	{
        $this->categoria = new Category();
  	}

  	public function saveCategory()
    {
        
        $categoria['nome'] = filter_input(INPUT_POST,'nome',FILTER_SANITIZE_STRING);
        $categoria['codigo'] = filter_input(INPUT_POST,'codigo',FILTER_SANITIZE_NUMBER_INT);

        $this->categoria->save($categoria);
         
        redirect("/category/categories");
    }

    public function editCategory()
    {
        $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
        $produto = $this->categoria->find(['id','=',$id]);
        $json = jsonDadosEncode($produto[0]);
        
        redirect("../category/editCategory?dados=".$json);   
    }

    public function updateCategory()
    {   
        $categoria['id'] = filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
        $categoria['nome'] = filter_input(INPUT_POST,'nome',FILTER_SANITIZE_STRING);
        $categoria['codigo'] = filter_input(INPUT_POST,'codigo',FILTER_SANITIZE_NUMBER_INT);

        $this->categoria->update($categoria);
        redirect("/category/categories");   
    }

    public function deleteCategory()
    {
        $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
        $produto = $this->categoria->delete($id);
        
        redirect("/category/categories");   
    }
     
}