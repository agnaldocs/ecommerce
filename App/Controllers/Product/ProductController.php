<?php
namespace App\Controllers\Product;


use App\Models\Product;
use App\Models\ProductCategory;
use App\Controllers\Controller;

class ProductController extends Controller
{

    private $produto;

  	public function __construct()
  	{
        $this->produto = new Product();
  	}

     
  	public function saveProduct()
    {   
        $produto['sku'] = filter_input(INPUT_POST,'sku',FILTER_SANITIZE_STRING);
        $produto['nome'] = filter_input(INPUT_POST,'nome', FILTER_SANITIZE_STRING);
        $produto['preco'] = filter_input(INPUT_POST,'preco', FILTER_SANITIZE_NUMBER_FLOAT);
        $produto['quantidade'] = filter_input(INPUT_POST,'quantidade', FILTER_SANITIZE_NUMBER_INT);
        $produto['descricao'] = filter_input(INPUT_POST,'descricao', FILTER_SANITIZE_STRING);
        $produto['url'] = importArquivo();

        //array de ids categoria
        $categoria_ids = $_POST['categoria_id'];


        if ( count($categoria_ids) > 0 ){
        
            $id = $this->produto->insertId($produto);
            if ( $id > 0 ){
                (new ProductCategory())->relacionamento($id,$categoria_ids);
              }
        }
    
        redirect("/product/products");
  	}
    
    public function edit()
    {
        $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
        $produto = $this->produto->find(['id','=',$id]);
        
        $json = jsonDadosEncode($produto[0]);
        redirect("../product/editProduct?dados=".$json);   
    }

    public function updateProduct()
    {
        $produto['id'] = filter_input(INPUT_POST,'id', FILTER_SANITIZE_NUMBER_INT);
        $produto['sku'] = filter_input(INPUT_POST,'sku',FILTER_SANITIZE_STRING);
        $produto['nome'] = filter_input(INPUT_POST,'nome', FILTER_SANITIZE_STRING);
        $produto['preco'] = filter_input(INPUT_POST,'preco', FILTER_SANITIZE_NUMBER_FLOAT);
        $produto['quantidade'] = filter_input(INPUT_POST,'quantidade', FILTER_SANITIZE_NUMBER_INT);
        $produto['descricao'] = filter_input(INPUT_POST,'descricao', FILTER_SANITIZE_STRING);
        $produto['url'] = importArquivo();
       
        $this->produto->update($produto);
        
        //array de ids de categoria
        $categoria_ids = $_POST['categoria_id'];

        if ( count($categoria_ids ) > 0 ){

            (new ProductCategory)->updateProdutoCategoria($produto['id'],$categoria_ids);
             
        }

        redirect("/product/products");

    }

    public function deleteProduct()
    {
        $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
        (new ProductCategory)->deleteProductCategory($id);
        $this->produto->delete($id);
        
        redirect("/product/products");   
    
    }

}