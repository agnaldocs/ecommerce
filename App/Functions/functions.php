<?php
use App\Models\ProductCategory;
use App\Models\Category;

function dd($dump){
    var_dump($dump);
    die();
}

function categoriaProdutos($id){
	
	$categorias = categoriasDeProdutos($id);
	$nome = "";
    foreach ($categorias as $key => $cat) {
        $nome .= $cat->nome." ";	
    }

    return $nome;

}

function categoriasDeProdutos($id){
	
	return (new ProductCategory)->categoriasDeProdutos($id);
	
}

function redirect($target){
    return header("location:{$target}");
}

function jsonDadosEncode($dados){
    return base64_encode(json_encode($dados));
}
function jsonDadosDecode($dados){
    return json_decode(base64_decode($dados));

}
function listaDeCategorias(){
    return (new Category)->all();
}

//recebe um arquivo tipo png 
//retorna path do arquivo
function importArquivo(){
   
    
    if ( $_FILES['arquivo']['type'] == 'image/png' ){

        $destino = 'assets/images/product/' . $_FILES['arquivo']['name'];
        $arquivo_tmp = $_FILES['arquivo']['tmp_name'];
        if ( move_uploaded_file( $arquivo_tmp, $destino ) ) {
            return $destino;
        } 
    }

    return null;

}