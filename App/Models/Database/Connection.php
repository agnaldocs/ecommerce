<?php

namespace App\Models\Database;

use PDO;

class Connection
{
 
    public static $conn;
 
    private function __construct() 
    {
        //
    }
 
    public static function connection($config) 
    {

        if (!isset(self::$conn)) {
            self::$conn = new PDO("mysql:host=localhost;dbname={$config['database']}",
            "{$config['username']}","{$config['password']}", array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));            
            self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$conn->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
            self::$conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        }
 
        return self::$conn;
    }

}