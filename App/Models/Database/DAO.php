<?php

namespace App\Models\Database;


class DAO
{
	private $connection;

	public function __construct() 
	{
		$this->connection = Bind::get('connection');
	}

	public function daoFind($sql) 
	{
		$find = $this->connection->prepare($sql);
		$find->execute();
		return $find->fetchall();
	}
    
    public function daoFindAll($sql) 
	{
		$findAll = $this->connection->prepare($sql);
		$findAll->execute();
		return $findAll->fetchall();
	}

	public function daoExecute($sql) 
	{
		try{
			$this->connection->beginTransaction(); 
			$execute = $this->connection->prepare($sql);
			$execute->execute();
			$rowCount = $execute->rowCount();
			$this->connection->commit();
			return $rowCount;
		} catch(PDOExecption $e) { 
			$this->connection->rollback(); 
			print_r("Error!: " . $e->getMessage()); 
			return 0;
		} 
	    
	}

	public function daoInsertId($sql) 
	{
		
		try { 
			$this->connection->beginTransaction(); 
     		$insert = $this->connection->prepare($sql);
			$insert->execute(); 
			$id = $this->connection->lastInsertId(); 
			$this->connection->commit();
			return $id; 
		} catch(PDOExecption $e) { 
			$this->connection->rollback(); 
			print_r("Error!: " . $e->getMessage()); 
			return 0;
		} 
	}
}	