<?php

namespace App\Models;


use App\Models\Database\DAO;


class Model extends DAO
{
    
    //recebe um array com 3 valores 1: coluna,2:operador,3:valor ex:['id','=','1']
    public function find($paramenters)
    {
        $sql = "select * from {$this->table}".$this->where($paramenters);
        return $this->daoFind($sql);
    }

    public function all()
    {
        $sql = "select * from {$this->table}";
        return $this->daoFindAll($sql);
    }
    
    //recebe um array com indices que se refere a coluna da tabela e valores
    //retorna um id 
    public function insertId($paramenters)
    {
        return $this->daoInsertId($this->insertSQL($paramenters));
    }

    //recebe um array com indices que se refere a coluna da tabela e valores
    //retorna um inteiro 0 ou 1
    public function save($paramenters)
    {
        return $this->daoExecute($this->insertSQL($paramenters));
    }

    //retorna uma sql montada de insert
    private function insertSQL($paramenters)
    {
        $sql = "insert into {$this->table}";
        $colunas = "(id";
        $valores = " values (null";
        foreach ($paramenters as $coluna => $valor) {
           $colunas .= ",{$coluna}";
           $valores .= ",'{$valor}'";
        }

        $sql .= $colunas.")".$valores.")";
        return $sql;
    }

    //retorna um inteiro 0 ou 1
    public function update($paramenters)
    {
        return $this->daoExecute($this->updateSQL($paramenters));
    }

    //retorna uma sql montada de update 
    public function updateSQL($paramenters)
    {
        $sql = "update {$this->table} set 0";
        $colunas = "";
        
        foreach ($paramenters as $coluna => $valor) {

            if ( $coluna != "id" ) {
                $colunas .= ", {$coluna}='{$valor}'";
            }
            
        }

        $sql .= $colunas." where id='{$paramenters['id']}'";
        $sql = str_replace("0,", "", $sql);
        //var_dump($sql);exit;
    	return $sql;
    }

    //retorna um inteiro 0 ou 1
    public function delete($paramenters)
    {
        
        $sql = "delete from {$this->table} where id={$paramenters}";

        return $this->daoExecute($sql);
    }
    
    //retorna uma string 
    private function where($paramenters)
    {
        $where = "";
        if( count($paramenters) > 1 ) {
            $where = " where ". implode(' ',$paramenters);
        }
        return $where;
    }

    public function query($sql){
        return $this->daoFindAll($sql);
    }

}
