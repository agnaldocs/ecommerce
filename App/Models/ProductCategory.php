<?php

namespace App\Models;


class ProductCategory extends Model
{

    protected $table = 'produto_categorias';

    public function relacionamento($id,$categorias)
    {
        foreach ($categorias as $key=>$value) {
            $this->save(['produto_id'=>$id,'categoria_id'=>$value]);
        }

    }

    public function categoriasDeProdutos($id)
    {
    	$sql = "select p.id,p.produto_id,p.categoria_id,c.nome,c.codigo 
    	from produto_categorias as p 
    	inner join categorias as c on (p.categoria_id = c.id)
    	where produto_id=".$id;
        return $this->query($sql);
    } 
    
    public function deleteProductCategory($id)
    {
         $res = 0;
         $produtoCategorias = $this->categoriasDeProdutos($id);
         
         foreach ($produtoCategorias as $produtoCategoria) {
             $res = $this->delete($produtoCategoria->id);

         }

         return $res;
    } 

    public function updateProdutoCategoria($produto_id,$categoria_ids)
    {

         $res = 0;
         $i = count($categoria_ids)-1;
        
         $produtoCategorias = $this->categoriasDeProdutos($produto_id);
         
         //para produto sem categorias criar as categorias do produto
         if ( empty($produtoCategorias) ){
             return $this->relacionamento($produto_id,$categoria_ids);
         }
   
         if ( count($produtoCategorias ) >= count($categoria_ids) ){
          
                foreach ($produtoCategorias as $produtoCategoria) {
                    
                    if( $i >= 0){
                        
                        $categoria_id = $categoria_ids[$i];
                        $res = $this->update([
                            'id'=>$produtoCategoria->id,
                            'produto_id'=>$produto_id,
                            'categoria_id'=>$categoria_id
                            ]);
                    }
                    
                    $i--;

                }
           

         }
         return $res;
    }

}