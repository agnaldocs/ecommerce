<?php 

namespace App\Models;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductCategory;


class Seed
{

	private $path;
	
	function __construct($path)
	{
		$this->path = $path;
 	}

	public function importar()
	{

		$file = $this->importarArquivoCSV();
        
        $qnt = count($file);
        $res = 0;
        for ( $i=0; $i < $qnt; $i++ ) { 
            
            if( $i > 0 ){

	            
	            $linha = $this->getLinha($file,$i);
                if ( count($linha) > 5 ){

				    $categorias = !empty($linha[5]) ? $linha[5] : array();
		             
				    $produto['nome'] = !empty($linha[0]) ? $linha[0] : null;
				    $produto['sku'] = !empty($linha[1]) ? $linha[1] : null;
				    $produto['descricao'] = !empty($linha[2]) ? $linha[2] : null;
				    $produto['quantidade'] = !empty($linha[3]) ? $linha[3] : 0;
				    $produto['preco'] = !empty($linha[4]) ? $linha[4] : 0.0;
				    
			        $categoria_ids = $this->salvarCategoria($categorias);
	               
			        $produto_id = $this->salvarProduto($produto);
		           
	 
			        $res += $this->salvarProdutoCategoria($produto_id,$categoria_ids);
                }
            }
        }

        return $res;
    }
    
    private function importarArquivoCSV()
    {

	    $file = fopen($this->path, "r");

	    $result = array();
	   
	    $i = 0;
	    while (!feof($file)){
	       
	        if (substr(($result[$i] = fgets($file)), 0, 10) !== ';;;;;;;;'){
	            $i++;
	        }
	    }

	    fclose($file);

        return $result;
    }
   
    private function getLinha($file,$id)
    {
    	
    	return explode(";", $file[$id]);
   
    }

    private function salvarCategoria($categorias)
    {
    	$ids = array();
    	
    	if ( count($categorias) == 0 ){
          return $ids;
    	}

    	$lista_categoria = explode("|", $categorias);
        

        foreach ($lista_categoria as $key => $nome) {
        	
        	$cod = rand(99, 1000);
            $ids[] = (new Category)->insertId(['codigo'=>$cod,'nome'=>$nome]);
        }

        return $ids;

    }
    
    private function salvarProduto($produto)
    {
    	return (new Product)->insertId($produto);

    }

    private function salvarProdutoCategoria($produto_id,$categoria_ids)
    {
    	$res = 0;
    	
    	if ( count($categoria_ids) == 0 ){
            return $res;   
    	}
    	foreach ($categoria_ids as $key => $categoria_id) {
    		$res = (new ProductCategory)->save(['produto_id'=>$produto_id,'categoria_id'=>$categoria_id]);
    	}

    	return $res;

    }


}