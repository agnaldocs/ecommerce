 <!--conteudo categoria-->
    <h1 class="title new-item">New Category</h1>

    <form action="/category/saveCategory" method="POST">

      <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input type="text" id="category-name" name="nome" class="input-text" />

      </div>
      <div class="input-field">
        <label for="category-code" class="label">Category Code</label>
        <input type="text" id="category-code" name="codigo" class="input-text" />

      </div>

      <div class="actions-form">
        <a href="/category/categories" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save" />
      </div>
    </form>
   <!--conteudo categoria-->