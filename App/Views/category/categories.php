<!--conteudo categoria-->
    <div class="header-list-page">
      <h1 class="title">Categories</h1>
      <a href="/category/addCategory" class="btn-action">Add new Category</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Code</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
      <?php foreach ($categorias as $categoria):?>
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$categoria->nome?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$categoria->codigo?></span>
        </td>

        <td class="data-grid-td">
          <div class="actions">
          <div class="action edit"><span><a href="/category/edit?id=<?=$categoria->id;?>">Edit</span></div>
          <div class="action edit"><span><a href="/category/delete?id=<?=$categoria->id;?>">Delete</span></div>
          </div>
        </td>
      </tr>
<?php endforeach?>
</table>
 <!--conteudo categoria-->