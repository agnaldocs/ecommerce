<?php 

use App\Models\Seed;


$seed = (new Seed('assets/import.csv'))->importar();

echo "<h1>Foram criados {$seed} registros</h1>";