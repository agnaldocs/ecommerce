<?php

 use App\Models\Product;
 
 $produtos = (new Product)->all();

?>

<div class="infor">
  You have <?=count($produtos)?> products added on this store: <a href="/product/addProduct" class="btn-action">Add new Product</a>
</div>
<ul class="product-list">
<?php foreach ($produtos as $key => $produto):?>
  <li>
    <div class="product-image">
      <img src="<?=$produto->url?>" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />
    </div>
    
    <div class="product-info">
      <div class="product-name"><span><?=$produto->nome?></span></div>
      <div class="product-price"><span class="special-price">9 available</span> <span>R$<?=number_format($produto->preco,2,',','.')?></span></div>
    </div>
      </li>
<?php endforeach ?>
</ul>