 <!--conteudo produto-->
    <h1 class="title new-item">New Product</h1>

    <form action="/product/saveProduct" method="POST" enctype="multipart/form-data">

      <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input type="text" id="sku" name="sku" class="input-text" />
      </div>
      <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" id="name" name="nome" class="input-text" />
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" id="price" name="preco" class="input-text" />
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="text" id="quantity" name="quantidade" class="input-text" />
      </div>
      <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple id="category" name="categoria_id[]" class="input-text">
          <?php 
          foreach($categorias as $categoria){
              echo "<option value=\"{$categoria->id}\">{$categoria->nome}</option>";
          }?>
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea id="description" name="descricao" class="input-text"></textarea>
      </div>

      <div class="input-field"  style="margin-left:250px;">
        
        <input type="file" class="input-text" id="arquivo" name="arquivo">
      </div>

      <div class="actions-form">
        <a href="/product/products/" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save Product" />
      </div>

    </form>
   <!--conteudo produto-->