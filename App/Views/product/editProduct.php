 <!--conteudo produto-->
 <?php 
    $produto = jsonDadosDecode($_GET['dados']);
    
?>
    <h1 class="title new-item">New Product</h1>

    <form action="/product/updateProduto" method="POST" enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?=$produto->id?>">
      <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input type="text" id="sku" name="sku" class="input-text" value="<?=$produto->sku?>" />
      </div>
      <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" id="name" name="nome" class="input-text" value="<?=$produto->nome?>"/>
      </div>
      <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" id="price" name="preco" class="input-text" value="<?=$produto->preco?>"/>
      </div>
      <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="text" id="quantity" name="quantidade" class="input-text" value="<?=$produto->quantidade?>"/>
      </div>
      <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple id="category" name="categoria_id[]" class="input-text">
          <?php
           
          $lista = categoriasDeProdutos($produto->id);
          $i = 0;
          $categorias = listaDeCategorias();
        //  var_dump($lista);
          
          foreach($categorias as $categoria){

              if ( $lista[$i]->categoria_id == $categoria->id ){
                echo "<option value=\"{$categoria->id}\" selected>{$categoria->nome}</option>";
              }else{
                echo "<option value=\"{$categoria->id}\">{$categoria->nome}</option>";
                
              }
              $i++;
          }?>
          
        </select>
      </div>
      <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea id="description" name="descricao" class="input-text"><?=$produto->descricao?></textarea>
      </div>

      <div class="input-field"  style="margin-left:250px;">
        
        <input type="file" class="input-text" id="arquivo" name="arquivo">
      </div>
      
      <div class="actions-form">
        <a href="/product/products" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Update" />
      </div>

    </form>
   <!--conteudo produto-->