 <!--conteudo produto-->
    <div class="header-list-page">
      <h1 class="title">Products</h1>
      <a href="/product/addProduct" class="btn-action">Add new Product</a>
    </div>
    <table class="data-grid">
      <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">SKU</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Price</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Quantity</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Categories</span>
        </th>

        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
      </tr>
      <?php foreach($produtos as $produto):
     //   var_dump($produto);
        ?>
      <tr class="data-row">
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$produto->nome?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$produto->sku?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$produto->preco?></span>
        </td>

        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=$produto->quantidade?></span>
        </td>
        
        <td class="data-grid-td">
           <span class="data-grid-cell-content"><?=categoriaProdutos($produto->id) ?></span>
        </td>

        <td class="data-grid-td">
          <div class="actions">
            <div class="action edit"><span><a href="/product/edit?id=<?=$produto->id;?>">Edit</a></span></div>
            <div class="action delete"><span><a href="/product/delete?id=<?=$produto->id?>">Delete</a></span></div>
          </div>
        </td>
      </tr>
<?php endforeach;?>
     
    </table>
   <!--conteudo produto-->