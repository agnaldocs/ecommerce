<?php 

namespace App\Models;

use App\Models\Database\Bind;
use App\Models\Database\Connection;
require '../config.php';

Bind::bind('connection', Connection::connection($config));

$r = Bind::get('connection');
var_dump($r);
