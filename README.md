# E-commerce

Projeto com implementação usando padrão de projeto

### Requisistos

PHP 7.0 ou >

MYSQL 5.0

APACHE 2


### Instalação
Criar banco de dados 

Configurar usuário e senha no arquivo App/config.php

Fazer migração através do endereço localhost/config/teste_migrate_import_db

Popular base de dados através do endereço localhost/config/teste_seed

Dar permissão de acesso a pasta public/assets/images para upload de imagens

Executar no terminal o comando composer dump-autoload


## Executar testes
Teste para conexão 

http://localhost/teste/teste_conexao

Teste de crud categoria

http://localhost/teste/teste_categoria

Teste de crud para produto

http://localhost/teste/teste_produto

Teste para importar arquivo sql e gerar as tabelas 

http://localhost/config/teste_migrate_import_db

Teste para importar arquivo local public/assets/import.csv e popular a base de dados com 10 registos

http://localhost/config/teste_seed

Teste para verificar objeto controlador produto

http://localhost/config/teste_controle_produto

Teste para verificar objeto controlador categoria

http://localhost/config/teste_controle_categoria

Teste para exportar o banco de dados

http://localhost/config/teste_migrate_dump_db.php


## Ferramentas Utilizadas
[Composer](https://getcomposer.org/)
[Sublime Text](https://www.sublimetext.com/)
[Mysql Workbench](https://www.mysql.com/products/workbench/)
[git](https://git-scm.com/)

## Tecnologia Utilizadas
[Padrão de Projeto MVC](https://pt.wikipedia.org/wiki/MVC)
[Padrão PSR](https://tableless.com.br/psr-padrao-para-codigo-php/)


## Observações
Para fazer upload de arquivos no cadastro de produtos o arquivo deve ser no formato PNG



## Autor

* **Agnaldo Correia** - *Initial work* - [bitbucket](https://bitbucket.org/agnaldocs/)


