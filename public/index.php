<?php

ini_set("display_errors",1);
ini_set("display_startup_erros",1);
error_reporting(E_ALL);

require "../vendor/autoload.php";
require "../config.php";
require "../App/Functions/functions.php";


use App\Models\Database\Bind;
use App\Models\Database\Connection;
use App\Models\Category;
use App\Models\Product;

Bind::bind('connection', Connection::connection($config));


$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

$categorias = (new Category)->all();

$produtos = (new Product)->all();

if($uri == "/"){
    $page = "/dashboard";
}else{
  $page = $uri;
}

$page .= ".php";

include('template.php');

//require "../App/Views".$page;