<!DOCTYPE html>
<html lang="en">

<head>
  <title>E-commerce Lojas Virtual</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <link  rel="stylesheet" type="text/css"  media="all" href="/assets/css/style.css" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
  <meta name="viewport" content="width=device-width,minimum-scale=1">
  <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
  <script async src="https://cdn.ampproject.org/v0.js"></script>
  <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script></head>
</head>

<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="/assets/images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="/dashboard"><img src="/assets/images/ecommerce.png" alt="Welcome" width="60" height="43" /></a>
  <div>
    <ul>
      <li><a href="/category/categories" class="link-menu">Categorias</a></li>
      <li><a href="/product/products" class="link-menu">Produtos</a></li>
    </ul>
  </div>
</amp-sidebar>
<!-- Header -->
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="/dashboard" class="link-logo">
    <img src="/assets/images/ecommerce.png" alt="Welcome" width="5" height="5" />
   </a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>    
</header>  
<!-- Header -->

<body>
  <!-- Main Content -->
  <main class="content">

    <div class="header-list-page">
      <h1 class="title"></h1>
    </div>

    <div class="container">
    <div class="row">
     <?php 

     require "../App/Views/".$page; 
     
     ?>
    </div>
  </div>

  </main>
  <!-- Main Content -->

  <!-- Footer -->
<footer>
	<div class="footer-image">
	  <img src="/assets/images/ecommerce2.png" width="110" height="50" alt="" />
	</div>
	<div class="email-content">
	  <span>developer@singledevs.com.br</span>
	</div>
</footer>
 <!-- Footer -->
</body>
</html>
